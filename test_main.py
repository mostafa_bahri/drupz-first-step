from main import maxDiff, solveForArray


def test_max_profit():
	"""
	Checking for the first condition:
	Maximum profit
	"""
	arr = [10, 3, 6, 18, 15, 2, 6, 8, 10]
	assert maxDiff(arr) == (15, 1, 3)

	arr = [11, 3, 10, 20, 20]
	assert maxDiff(arr) == (17, 1, 3)


def test_smallest_timeframe():
	"""
	Testing for the second condition, given equal results
	for the last condition:
	Smallest(min) buy/sell timeframe"""
	arr = [10, 8, 5, 7, 6, 12, 5, 7, 8, 12, 4, 8]
	assert maxDiff(arr) == (7, 2, 5)


def test_earliest_time():
	"""
	Testing for the third condition, given equal results
	for the last condition:
	Earliest time to buy"""
	arr = [1, 3, 1, 3, -1, 1]
	assert maxDiff(arr) == (2, 0, 1)

	arr = [2, 2, 2, 2, 2]
	assert maxDiff(arr) == (0, 0, 1)


def test_input_consecutive_duplicates_removed():
	arr = [10, 7, 5, 8, 7, 10, 11, 9]
	assert maxDiff(arr) == (6, 2, 6)


def test_input_consecutive_duplicates_reduced():
	arr = [10, 10, 7, 7, 5, 5, 8, 8, 7, 7, 10, 10, 11, 11, 9, 9]
	assert maxDiff(arr) == (6, 5, 12)


def test_strictly_increasing():
	arr = [1, 2, 3, 4, 5]
	assert maxDiff(arr) == (4, 0, 4)

	arr = [1, 3, 10, 20]
	assert maxDiff(arr) == (19, 0, 3)


def test_some_mixture():
	arr = [8, 19, 10, 2, 3]
	assert maxDiff(arr) == (11, 0, 1)

	arr = [8, 8, 8, 19, 19, 19, 10, 2, 3]
	assert maxDiff(arr) == (11, 2, 3)

	arr = [10, 13, 6, 3, 8, 13, 5, 6, 2, 9]
	assert maxDiff(arr) == (10, 3, 5)


def test_solve():
	arr = [10, 10, 7, 7, 5, 5, 8, 8, 7, 7, 10, 10, 11, 11, 9, 9]
	assert solveForArray(arr) == (6, '08:05', '08:12')

	arr = [1, 2, 3, 4]
	assert solveForArray(arr) == (3, '08:00', '08:03')

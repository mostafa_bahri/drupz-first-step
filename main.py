from collections import namedtuple
from datetime import datetime, timedelta
from math import inf


def solveForArray(input_array):
	"""
	The entry point of the algorithm

	:param input_array:
	:return: output 3 element tuple
	"""
	res = maxDiff(input_array)
	return mapToOutputFormat(res)


def mapToOutputFormat(res):
	"""
	a simple mapping to get the desired output
	format
	:param res: `RawDiff` instance
	:return: mapped tuple
	"""
	return (res.max_diff, getTime(res.min_index),
			getTime(res.max_index))


# utility tuple wrapping `maxDiff` return
RawDiff = namedtuple(
	'RawDiff', [
		'max_diff', 'min_index', 'max_index'])

POSTIVE_INF = inf
NEGATIVE_INF = -inf


def maxDiff(arr):
	"""
	The core algorithm finding the max difference
	hence the maximal profit


	:return: `RawDiff` instance
	"""
	min_index, max_index = POSTIVE_INF, NEGATIVE_INF
	best_min_index = POSTIVE_INF

	max_diff = NEGATIVE_INF
	best_min = POSTIVE_INF  # holding current best found minima

	for i in range(0, len(arr)):
		# strictly greater than to obtain least distance(no shift)
		if arr[i] - best_min > max_diff:
			max_diff = arr[i] - best_min
			max_index = i

			# `best_min_index` could differ from `min_index``
			# ina a case like : [10,19,3] where the global minima(3)
			# is after the minima (10) leading to the maximal difference (19-10)
			# so `min_index` is only updated when it's taken to be the
			# contributing element to the maximal difference
			min_index = best_min_index

		# equal less than to obtain least dstance as well (shift forward)
		if arr[i] <= best_min:
			best_min = arr[i]
			best_min_index = i

	return RawDiff(max_diff, min_index, max_index)


def getTime(time_index):
	"""
	Get time representation in `HH:MM` format
	based on input. Time indexed from 8:00 AM by 1 minute

	:return str
	"""
	dt = datetime(2018, 1, 1, 8, 0)
	time = dt + timedelta(minutes=time_index)
	return time.strftime('%H:%M')


if __name__ == '__main__':
	arr = [10, 10, 7, 7, 5, 5, 8, 8, 7, 7, 10, 10, 11, 11, 9, 9]
	print(solveForArray(arr))

FROM python:3.7-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt


CMD ["pytest"]

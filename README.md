# Drupz Internship Step 1 
> Mostafa Bahri

## Part One

The algorithm, in essence efficiently finds the maximum difference between two elements in the array such that the greater (greater equal to be precise ) element appears only after the  other one. 

Implementation details are covered in the source code `main.py`

The rationale behind this approach is first, obviously, we are aiming for the maximum profit. Secondly and importantly, we need to buy *before* selling; hence the necessity that the smaller element (buy time) **must** appear before the larger one (sell time).

The algorithm complexity follows:

	Time Complexity: O(n)
	Space Complexity: O(1)

### requirements
`python > 3.4`

### installation
`pip install -r requirements.txt`

### tests
Run in the current directory:

`pytest`

A plethora of test cases can be found under `test_main.py` covering a large amount of input possibilities. I've tried to cover as far as possible. 

The actual sample test case is accessible in `test_sample.py`



## Part Two
The approach seems to be of a greedy nature. We try to trade on all or most local *extrema* - buy a share at a local minima then sell it at the next coming local maxima.

The best answer, however, is not guaranteed. Honestly that's just purely my speculation as of now. I need to investigate it further.    
*No code is present for this part.*

## Part Three
At this point, it sounds more like a classic regression problem. We try to predict the future stock prices given features extracted from last year and currently-being-fed data. Then we aim for the most potentially profitable trades.

That's as far as I can conjecture. I'm quite new to the world of ML, and humbly I admit my ignorance about the subject.    
*No code is present for this part*.


